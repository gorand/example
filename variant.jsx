var Variant = React.createClass({

  handleChange: function(e) {
    var _id = +e.target.htmlFor,
        _checked = +e.target.previousSibling.checked,
        _correct = this.props.correctVariant;

    if (!this.props.selectedVariant) {
      this.props.changeAnswer(_id, _correct);
      _checked = '';
    }
    
    return false;
  },

  setClassMessage: function(index, value) {
    return index === value ? ' is-correct' : ' is-wrong';
  },

  setStatusMessage: function(index, value, selected) {
    if (index === value && selected !== value) {
      return null;      
    } else if (index === value) {
      return 'Верно';
    } 

    return 'Неверно';
  },

  render: function() {
    var textAnswer,
        idVariant = this.props.item.idVariant,
        correctVariant = this.props.correctVariant,
        selectedVariant = this.props.selectedVariant;

    if (!selectedVariant) {
      textAnswer = null
    } else if (idVariant === correctVariant || idVariant === selectedVariant) {
      textAnswer = (
        <div className={'quest-answer' + (this.setClassMessage(idVariant, correctVariant))}>
          <span className='quest-answer-value'>{this.setStatusMessage(idVariant, correctVariant, selectedVariant)}</span>
          <span className='quest-answer-note'>{this.props.item.message}</span>
        </div>
      )
    } 

    return(
      <div className='form-field'>
        <input className='form-radio' checked='' type='radio' name='radio' id={idVariant} />
        <label 
          onClick={this.handleChange} 
          className={'form-radio-label' + (selectedVariant ? ' is-disable' : '')} 
          htmlFor={idVariant}
        >
          {this.props.item.title}
        </label>        
        {textAnswer}
      </div>
    );
  }
});
