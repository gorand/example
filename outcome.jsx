var Social = React.createClass({
  render: function() {
    var name = this.props.item.name,
        alias = this.props.item.alias,
        url = 'https://share.yandex.net/go.xml?service=' + alias 
            + '&amp;url=' + this.props.url  
            + '&amp;title=' + this.props.title;

    return (
      <span className="b-share-btn__wrap">
        <a 
          className={'b-share__handle b-share__link b-share-btn__' + alias} 
          href={url} 
          data-service={alias} 
          title={name} 
          rel="nofollow" 
          target="_blank"
        >
          <span className={'b-share-icon b-share-icon_' + alias}></span>
        </a>
      </span>
    );
  }
});

var Outcome = React.createClass({
  socials: [
    {
      alias: 'vkontakte',
      name: 'ВКонтакте'
    },
    {
      alias: 'facebook',
      name: 'Facebook'
    },
    {
      alias: 'odnoklassniki',
      name: 'Одноклассники'
    }
  ],

  render: function() {
    var url = window.location.href,
        result =  this.props.correct + ' из ' + this.props.total, 
        title = 'Мой результат ' + result + ' в тесте от Sibnovosti.ru: ' + this.props.title,
        info = this.props.info;

    var socialBtn = this.socials.map(function(_item, _key) {
      return <Social
        url={url}
        title={title}
        key={_key}
        item={_item} />
    });

    return (
      <div>
        <div className='quest-outcome'>
          <h2>Ваш результат</h2>
          <div className='quest-outcome-title'>{result}</div>
          <b className='quest-outcome-info'>Поделиться:</b>
          <div className="share-wrap">
            <div 
              className="yashare-auto-init b-share_theme_counter" 
              data-yasharel10n="ru" 
              data-yasharequickservices="vkontakte,facebook,odnoklassniki" 
              data-yasharetheme="counter" 
              data-yasharetype="icon" 
              data-yasharelink={url}
            >
              <span className="b-share">
                {socialBtn}              
              </span>
            </div>
          </div>
        </div>
        <div className={'quest-notice ' + (info.length ? '' : 'hide')}>
          <h3 className='quest-notice-text'>{info}</h3>
        </div>
      </div>
    )
  }
});
