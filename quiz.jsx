var Quiz = React.createClass({
  getInitialState: function() {
    return { 
      selectedVariant: null,
      indexQuiz: 0,
      totalCorrect: 0,
      imageUrl: null,
      isOutcome: false,
      initStart: false,
      length: this.props.length
    };
  },

  handleStartQuest: function() {
    this.setState({
      initStart: true
    })
  },

  handleNextQuiz: function(e) {
    e.preventDefault();
    this.setState({
      indexQuiz: this.state.indexQuiz + 1, 
      selectedVariant: null
    });
  },

  submitVariant: function(index, correct) {
    this.setState({
      selectedVariant: index
    });

    if (index === correct) {
      this.setState({ 
        totalCorrect: this.state.totalCorrect + 1 
      });
    } 

    if (this.state.indexQuiz === (this.state.length - 1)) {
      this.setState({
        isOutcome: true
      });
    }
  },

  render: function() {
    var title = this.props.data.title,
        info = this.props.data.description,
        initStart = this.state.initStart,
        indexQuiz = this.state.indexQuiz,
        currentQuiz = indexQuiz + 1,
        totalQuiz = this.state.length,
        totalCorrect = this.state.totalCorrect,
        selectedVariant = this.state.selectedVariant,
        beforeOutcome = currentQuiz <= totalQuiz,
        titleQuiz = (beforeOutcome && initStart) ? this.props.data.questions[indexQuiz].title : null, 
        defaultImageUrl = beforeOutcome ? this.props.data.questions[indexQuiz].picture : null, 
        correctImageUrl = beforeOutcome ? this.props.data.questions[indexQuiz].correctPicture : null, 
        variants = beforeOutcome ? this.props.data.questions[indexQuiz].variants : [];

    var start,
        stage,
        list,
        outcome,
        picture;

    if (!initStart) {
      start =  <button onClick={this.handleStartQuest} className='quest-btn'>Начать тест</button>;
    } else if (!this.state.isOutcome || indexQuiz !== totalQuiz) {     
      if (defaultImageUrl && !selectedVariant || !correctImageUrl) {
        picture = <img src={defaultImageUrl} />;
      } else if (correctImageUrl && selectedVariant) {
        picture = <img className='quest-img-correct' src={correctImageUrl} /> ;
      } 
      else {
        picture = null;
      }

      stage = <div className='quest-stage'>{ currentQuiz + ' / ' + totalQuiz }</div>;

      list = variants.map(function(_item, _key) {
        return(
          <Variant
            changeAnswer={this.submitVariant}
            correctVariant={this.props.data.questions[indexQuiz].correctVariant}
            selectedVariant={selectedVariant}
            key={_key} 
            item={_item} />
        );
      }.bind(this));
    } else {
      outcome = <Outcome correct={totalCorrect} total={totalQuiz} info={info} title={this.props.data.title} />;
    }

    return (
      <div className='quest'>
        <div className='quest-header'>
          <h2>{title}</h2>
          {start}
          {stage}
          <h3 className='quest-title'>{titleQuiz}</h3>
          {picture}
        </div>
        <form className='form'>
          {list}
          {outcome}
          <button 
            onClick={this.handleNextQuiz} 
            className={'quest-btn' + (!selectedVariant ? ' hide' : '')}>
              Далее
          </button>
        </form>
      </div>
    );
  }
});
