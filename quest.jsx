var Quest = React.createClass({
	getInitialState: function() {
		return { 
      data: {
        title: null,
        description: null,
        questions: []
      }
		};
	},

	componentDidMount: function() {

    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        if ($.isEmptyObject(data)) {
          this.setState({ data: this.state.data });
        } else {
          this.setState({data: data});
        }
        localStorage.setItem('questoin', data);
      }.bind(this),
      error: function(xhr, status, err) {
        if (this.props.url) {
          console.error(this.props.url, status, err.toString());
        }
      }.bind(this)
    });
  },

  render: function() {
    if (this.state.data.questions.length) {
      return <Quiz data={this.state.data} length={this.state.data.questions.length}/>;
    } 
    return null;
  }
});
